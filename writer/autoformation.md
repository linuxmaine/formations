Avertissement

Cette auto-formation à Libre Office a été conçue avec la version 6.0.7.3 de ce programme sous la distribution Xubuntu 18.04 LTS ; utilisée dans un autre environnement, il est possible que certaines différences apparaissent.

Elle a été adaptée à partir de la formation « face à face » proposée par LinuxMaine, afin de permettre aux personnes qui l’ont suivie , de retravailler certains points. 



Qu’est-ce que Libre office ?

Il s’agit d’une suite1 bureautique Libre & Open Source ; elle est composée de Writer, le traitement de texte, de Calc le tableur, d’Impress le module de présentation, de Draw l’application de dessin et d'organigrammes, de Math l'éditeur de formules mathématiques et de Base la base de données.

LibreOffice est compatible avec de nombreux formats de document d’autres distributions et propose en plus le format OpenDocument (ODF). Par ailleurs un grand nombre d’extensions (plug-in) permet d’élargir les fonctions/commandes de base et les modèles de documents proposés. 

			 Le lanceur général de la suite


Il ne présente que peu d’intérêt, la majorité des commandes qu’il propose se retrouvant dans les logiciels composant la suite. La plupart du temps, le lancement du logiciel voulu se fera par l’icône spécifique installée sur le bureau. 

Présentation des 23 commandes communes aux 3 logiciels étudiés
Dans la barre supérieure haute des menus
Writer :
Calc :
Impress :
Dans la barre supérieure médiane des icônes
Writer : 
Calc :
Impress : 

(description effectuée de gauche à droite)

Barre supérieure haute

Fichier : 	menu permettant l’accès aux diverses opérations réalisables sur un fichier.
Edition : 	menu permettant l’accès aux diverses opérations d’édition réalisables sur un 
               	texte.
Affichage :	menu de paramétrage pour l’affichage du texte.
Insertion : 	menu permettant l’insertion de tout ce qui est « insérable » dans le texte.
Format :    	menu permettant le paramétrage de tout ce qui touche à la mise en forme du texte.
Outils :      	menu permettant la mise en œuvre de différents outils opérant sur le texte.
Fenêtre :   	menu permettant la gestion des fenêtres.
Aide :        		menu ouvrant l’accès à différentes sources d’aides et d’informations.

Barre supérieure médiane

(entre parenthèses le raccourci clavier quand il existe)

		(Ctrl+N) « Nouveau ». Créer un nouveau document.

		(Ctrl+O) Ouvrir un fichier local ou distant.

		(Ctrl+S) Enregistrer le document actif.

		Exporter au format pdf.

		(Ctrl+P) Imprimer.

		(Ctrl+X) Couper.

		(Ctrl+C) Copier.

		(Ctrl+V) Coller.

		Cloner le formatage.

		Annuler (dernière action).

		Rétablir (dernière action).

		(Ctrl+H) Rechercher et remplacer.

		(F7) Vérifier l’orthographe.

		(Ctrl+K) Insérer un hyperlien.

		Afficher les fonctions de dessin.






Travaux pratiques

 Traitement de texte

## Exercice 1 : Créer et sauvegarder un texte quelconque dans un fichier.  

Commandes utilisées : icône Writer, Fichier/enregistrer (ou enregistrer sous), Créer un dossier, Fichier/fermer, Fichier/ouvrir, Fichier/nouveau, Supprimer (un fichier), Restaurer, Renommer

Remarque : Un rangement organisé des fichiers créés est impératif !


Application : Lancer Writer par son icône depuis le tableau de bord, écrire une ligne de texte puis enregistrer le fichier dans le répertoire « Documents/Autoformation Writer » sous le nom « Writer-1.odt ». Quitter le logiciel Writer. 

Aller dans Répertoire/Documents/Autoformation Writer et ouvrir par 2-clics le fichier « Writer-1.odt » ; modifier la ligne écrite précédemment puis, par la commande « Fichier/Enregistrer sous », enregistrer le nouveau texte sous le nom « Writer-1-1.odt » ; fermer le fichier « Writer-1-1.odt ». 

Retourner maintenant dans « Répertoire/Documents/Autoformation Writer », sélectionner le fichier « Writer-1.odt » par un « clic gauche » puis actionner la touche « Suppr » du clavier.

« Oh ! Je me suis trompé ... ». Restaurer le fichier « Writer-1.odt » en ouvrant la « Corbeille » par un « clic droit », et en faisant « clic droit » sur le fichier « Writer-1.odt » puis « Restaurer ». 

Supprimer maintenant « Writer-1-1.odt » par « Clic droit/Supprimer ».
Vérifier qu’il n’est pas dans la corbeille (vous noterez bien la différence entre la première méthode (clic gauche + touche « Suppr ») qui met le fichier à la corbeille et la deuxième (Clic droit/Supprimer) qui détruit le fichier définitivement. 

Renommer enfin « Writer-1.odt » en « Exercice-TT1.odt » par « clic droit » puis « Renommer ».

Fermer toutes les fenêtres.


## Exercice 2 : Créer un texte à partir d’une mise en forme pré-existante (Lettre commerciale).


Nouvelles commandes utilisées : Nouveau/modèles, Insertion/Forme, Copier/coller (une image). 


Application : Lancer Writer depuis le tableau de bord.

Dans l’ascenseur de l’icône « Nouveau », aller sur « Modèles » puis choisir la lettre commerciale d’en haut à droite (Lettre commerciale moderne serif) et « Ouvrir ».

Sauvegarder tout de suite dans « Documents/Autoformation Writer/Exercice-TT22 »en « Exercice-TT2.odt». 

Remplir les zones entre < > avec vos données et régler correctement le format de la date (2-clic ...).
 
« 2-cliquer » sur le logo de gauche et effacer le texte ; à la place :

- après avoir sélectionné le dessin d’une étoile à 6 branches (par « Insertion/formes/étoiles/étoile à 6 branches » ), insérer à la souris en partie haute une étoile en plaçant le pointeur de souris à l’endroit voulu + clic gauche et déplacement de la souris pour régler la taille,

- puis insérer en partie basse l’image « linuxmaine » fournie (elle se trouve dans le fichier « Documents/ Autoformation Writer/Données-exercice-TT2/Exercice-TT2-image.odt » et est à insérer par un copier/coller – ajuster la taille après le « coller » si nécessaire). 


Sauvegarder à nouveau puis quitter le fichier. 

Pour terminer, ranger le dossier  « Données-exercice-TT2 » avec son contenu dans le dossier  « Exercice-TT2 » que vous avez créé juste avant.

## Exercice 3 : Rectifier des erreurs de manipulation dans un fichier, rechercher un mot, remplacer un mot utilisé plusieurs fois en un seul coup, insérer des notes.


Nouvelles commandes utilisées : Annuler dernière action/Rétablir dernière action, Rechercher (un mot), Rechercher et remplacer (un mot), Insertion/note.


Application : Dans répertoire/Autoformation Writer/Exercice-TT3, Lancer  le fichier Writer fourni « Exercice-TT3-1.odt ». 

Sélectionner à la souris le 2ème paragraphe, puis le couper. 

Sélectionner maintenant ce qui était le 3ème paragraphe à la souris, le copier puis le coller avant le premier paragraphe. 

Vous venez de perdre la totalité du 2ème paragraphe précédemment mémorisé. 

Vous pouvez remettre tout en ordre grâce à la fonction « Annuler » 
accessible en icône dans la barre supérieure médiane. 

Cliquer sur la flèche jusqu’à retrouver la configuration initiale des 3 paragraphes.




Rechercher maintenant le mot Titus par « Edition/Rechercher »3. Faire défiler le curseur sur toutes les occurrences de ce mot par l’une ou l’autre des flèches verticales (˅˄) situées à droite de la fenêtre de recherche.

Remplacer ensuite tous les « Titus » par : « TITUS », en une seule fois. Pour ce faire, utiliser la fonction « Rechercher & remplacer » du menu « Edition ».





Insérer enfin, après le « TITUS » du titre, en note de bas de page (par la commande Insertion/note de …) le texte suivant: « Traduction française de M. Cabaret-Dupaty, Paris, 1893, avec quelques adaptations de J. Poucet, Louvain, 2001 ». 

Remarque : pour accéder directement à une note de bas de page, il suffit de cliquer G sur le numéro de la note ; une fois « dans » la note, le retour à l’endroit de départ dans le texte est obtenu en cliquant sur le numéro dans la note cette fois.


Enregistrer votre travail sous le nom « Exercice-TT3-2.




## Exercice 4 : Structurer et mettre en forme un texte (aligner, centrer, mettre en gras / italique / souligné / couleur, police/taille de police, surligner, insérer un commentaire, ajuster les interlignes).


Nouvelles commandes utilisées : Icônes d’alignement (droit, centre, gauche, justifié), de mise en forme des caractères (gras, italique, souligné, exposant, indicé), Fenêtres de police et de taille, Couleur de mise en évidence, Insertion/commentaire, Icône « Définir interligne, Augmenter l’espacement ... ».


Application : Note : Dans cet exercice, nous n'utiliserons que les icônes des barres d'outils. 
2 fichiers sont fournis : « Exercice-TT4-1.odt » et « Modèle-TT4.odt ».


Lancer  le fichier Writer « Exercice-TT4-1.odt » ; vous pouvez voir qu’il s’agit d’un texte « brut » pas encore mis en forme. Lancer maintenant « Modèle-TT4.odt » afin de voir ce à quoi vous devrez arriver après mise en forme du texte « brut ».


Commencer par mettre l’ensemble du texte en « Freeserif 14 », à le découper (retours à la ligne) et à le disposer à sa place (Icônes d’alignement droit-centre-gauche) ; les inter-lignes seront réglés à « 1,5 » pour toute la feuille. 

Sauvegarder (au bon endroit) en « Exercice-TT4-2 ».

- Mettre la police du titre en « Freeserif 20 », noir, gras, souligné centré sur fond de couleur verte (le vert de votre choix).

- Régler ensuite la taille des 4 parties à surligner du texte à 16/italique et effectuer le surlignage en bleu turquoise (de votre choix) dans la foulée.

- Mettre les 2 parties de textes concernées en exposant et en indicé par les icônes de la barre supérieure basse                                  .

- Justifier le paragraphe «  A  20 h applaudir aux fenêtres … /… nous. » par l’icône de la barre supérieure basse ou par « Ctrl+j ».

- Insérer et saisir les 5 « commentaires » (par la commande Insertion/commentaire).

Sauvegarder puis fermer toutes les fenêtres.

 
## Exercice 5 : Numéroter les titres des paragraphes/chapitres d’un document et créer automatiquement une table des matières.

Nouvelles commandes utilisées : Numérotation des chapitres, Insérer une table des matières.

Introduction 
La fonction « Table des matières » de Writer permet de créer une table des matières automatisée à partir des « Titres » des paragraphes ou chapitres d’un document, titres qui seront automatiquement repris sous forme d’hyperliens dans cette table (ainsi, à chaque modification d’un « Titre », il y aura retranscription dans la table des matières dès la mise à jour suivante).
Cet exercice va vous permettre :
	• de paramétrer les « Titres » et de transformer les intitulés de paragraphes en
              « Titres » numérotés automatiquement,
	• d’insérer rapidement une table des matières avec les réglages par défaut.

Préparation 
Dans Répertoire/Autoformation Writer/Exercice TT-5, lancer  le fichier Writer « Exercice-TT5 ». Il s’agit d’un texte dont les chapitres ne sont pas numérotés et qui n’a pas encore de table des matières. 

Enregistrer ce fichier sous « Exercice-TT5-bis » ; vous allez l’utiliser pour les applications qui vont vous être proposées.
Etape 1 ( ! lisez cette note 4)
Dans « Exercice-TT5-bis », vous allez vérifier que les différents styles de « Titres » (1, 2, 3, etc …) sont paramétrés comme on a besoin, pour la numérotation (exemple : on souhaite obtenir une numérotation de type : 1 -, 1.1 -, 1.2 -, 1.3 -, etc.), et pour la position (c’est à dire le retrait du sous-titre par rapport au titre). 
Application 1 : Dans « Outils/numérotation des chapitres », paramétrez l’onglet « Numérotation »  de la façon suivante :
	- pour « Style de § » : « Niveau 1 » ==> « Titre 1 », « Niveau 2 » ==> « Titre 2 » et
	  « Niveau 3 » ==> « Titre 3 »,
	- pour « nombre » ==> sur « 1, 2, 3 » pour les 3 niveaux (1, 2 et 3),
	- pour « style de caractère » ==> « aucun » pour les 3 niveaux (1, 2, 3),
	- pour « Afficher les sous-niveaux » : « Niveau 2 » ==> 2 et « Niveau 3 » ==> 3,
	- pour « Séparateur/Après » ==> appuyez une fois sur la barre d’espace et insérez le tiret
	  du 6 pour chacun des niveaux 1, 2 et 3,
puis, dans l’onglet « Position » réglez le « retrait » du niveau 2 à « o,5 cm » et celui du niveau 3 à « 1 cm ». Quittez par « OK ».
 Etape 2
La commande pour transformer un intitulé de paragraphe en « Titre » numéroté automatiquement se trouve dans la fenêtre de gauche  (style de paragraphe) de la barre supérieure basse.    
Pour la mise en œuvre de cette commande, il suffit de placer le curseur sur la ligne de texte à transformer et de choisir avec l’ascenseur de la fenêtre  le type de « Titre » voulu ; la numérotation s’incrémentera alors automatiquement et le « Titre » sera par la suite affichable dans la tables des matières.

Application 2 : Transformez les lignes :

	- « Les fonctions  Numérotation des chapitres et Table des matières » en Titre principal,
	- « Introduction », « Préparation » et « Insertion de la table des matières » en Titre 1,
	- « Etape 1 » et « Etape 2 » en Titre 2.

Insertion de la table des matières
La plupart du temps, vous trouverez probablement que la table des matières par défaut est celle qui vous convient. En insérer une est très simple :
Application 3 :

1) Cliquez dans le document à l’emplacement souhaité pour la table des matières (en général au début ou à la fin du texte ; ici ce sera à la fin).

2) Sélectionnez Insertion/Table des matières et index/Table des matières, index ou bibliographie…

3) Cliquez sur OK. La table est insérée !

Remarques complémentaires
Remarque 1
La table des matières apparaît sur un arrière-plan gris qui sert à vous rappeler qu’elle est générée automatiquement, mais qui ne sera pas imprimé et qui n’apparaîtra pas si le document est converti au format PDF. Si vous voulez désactiver ce fond grisé, sélectionnez Outils > Options > LibreOffice > Couleurs de l’interface et décochez l’option Trame de fond des index et tables dans la section Document texte. Notez que cette modification peut laisser un fond gris derrière les points, entre les titres et les numéros de page car ceux-ci font partie d’une tabulation ; pour les désactiver eux aussi, sélectionnez Outils > Options > LibreOffice Writer > Aides au formatage et décochez l’option concernant les tabulations. 
Application 4 : testez la première de ces deux commandes puis remettez le fond grisé.
Remarque 2
Si l’option Édition > Suivi des modifications > Afficher est activée, quand vous modifiez un document dont vous mettez la table des matières à jour, des erreurs peuvent survenir, car la table inclura encore les titres supprimés, et les modifications effectuées sur le texte peuvent induire une mauvaise numérotation des pages. Pour éviter le problème, assurez-vous que cette option est désactivée avant de mettre la table des matières à jour.
Application 5 : désactivez cette option avant de passer à la remarque 3.

Remarque 3
Si par la suite vous ajoutez ou supprimer du texte, de telle manière que certains titres passent à une autre page, ou si vous ajoutez, supprimez ou modifiez des titres, vous devez mettre à jour la table des matières. Pour cela, faites un clic droit n’importe où dans la table et sélectionnez Mettre à jour l’index dans le menu contextuel. 
Application 4  : Copier le texte suivant (en grisé) et collez-le dans « Exercice-TT5.bis », à la fin du texte, comme  nouveau paragraphe :   
Remarque : 
Si par la suite vous ajoutez ou supprimer du texte, de telle manière que certains titres passent à une autre page, ou si vous ajoutez, supprimez ou modifiez des titres, vous devez mettre à jour la table des matières. Pour cela, faites un clic droit n’importe où dans la table et sélectionnez Mettre à jour l’index dans le menu contextuel. 
Transformez ensuite le «Remarque : » de ce dernier paragraphe en « Titre 2 » puis mettez à jour l’index (c’est à dire la table des matières) avec la méthode donnée juste avant.
Nota : Si vous n’arrivez pas à placer le curseur dans la table des matières, sélectionnez Outils > Options > LibreOffice Writer > Aides au formatage et cochez l’option Activer le curseur dans la section Zones protégées. 
Remarque 4
Si certains titres n’apparaissaient pas dans la table des matières, assurez-vous qu’ils possèdent bien un style de paragraphe convenable en revérifiant les réglages dans Outils > Numérotation des chapitres (vous avez dû oublier de paramétrer quelque chose). 

## Exercice 6  : Insérer un tableau de données et un diagramme dans un document texte.


Nouvelles commandes utilisées : Tableau/insérer un tableau, Insertion/diagramme.

Application : Dans Répertoire/Autoformation Writer/Exercice TT-6, lancer  le fichier Writer « Exercice-TT6 ». Vous trouverez en haut de la première page un ensemble de données qui vont servir à remplir le tableau que vous allez créer.

Placer le curseur à environ 3 retours à la ligne après le texte des données, puis par la commande « Tableau/Insérer un tableau », régler le nombre de colonnes à « 5 », celui des lignes à « 10 » et demander « Insérer ». Enregistrer votre travail sous « Exercice TT-6-bis ».

Remplir maintenant le tableau avec les données fournies en haut de la feuille en suivant les consignes indiquées avec les données. Enregistrer.

Nous allons maintenant insérer un diagramme utilisant les données de ce tableau et montrant graphiquement l’évolution des parts de marché des navigateurs internet analysés.

Placer le curseur n’importe où à l’intérieur du tableau, puis après avoir sélectionné la commande « Insertion/diagramme » (soit par le menu, soit par l’icône                                    ) , suivre les instructions proposées par l’assistant de diagramme :

	Etape 1 : type de diagramme : dans la 2ème colonne, cliquer sur « ligne » et sélectionner dans la fenêtre de droite le 3ème schéma en partant de la gauche ; cliquer sur suivant.

	Etape 2 : plage de données : la présélection automatique est normalement correcte si vous avez suivi les indications : les données du tableau (à A, B, C, D colonnes -  et à 1, …, 10 lignes) sont bien en colonne et les « étiquettes » seront bien les indications de la première ligne (Internet Explorer, …, Safari) et de la première colonne (2016, …, 2008) ; cliquer sur suivant.

	Etape 3 : Séries de données : On peut vérifier ici si les séries de données prises en compte à l’étape précédente sont correctes ; il suffit de se placer sur le nom de l’un des navigateurs de la fenêtre centrale « série de données » pour voir dans les fenêtres de droite les séries de cases correspondantes du tableau ; cliquer sur suivant.

	Etape 4 : On va pouvoir donner un titre au tableau et aux 2 axes « horizontal » et « vertical » : saisir dans « Titre » : « Parts de marché des navigateurs internet », dans « Axe x » : « Année » et dans « Axe y » : « % » puis cliquer sur « Terminer ».

Vous avez inséré un type de diagramme correspondant aux données du tableau. 

Enregistrer puis fermer toutes les fenêtres.

## Exercice 7 : Capturer une zone de l’écran et l’insérer dans un texte, ancrer cette image dans le document de différentes façons ; insérer un hyperlien.

Nouvelles commandes utilisées : « Capture d’écran »5, « Format/Ancre », « Insertion/hyperlien ».

Application 1 : Dans Répertoire/Autoformation Writer/Exercice TT-7, lancer  le fichier Writer « Exercice-TT7-1». Suivez les informations et les consignes qui sont directement dans le texte du document.

Application 2 : Dans Répertoire/Autoformation Writer/Exercice TT-7, lancer  le fichier Writer « Exercice-TT7-2». Les informations et les consignes sont directement dans le texte du document.

Application 3 : Dans Répertoire/Autoformation Writer/Exercice TT-7, lancer  le fichier Writer « Exercice-TT7-3». Les informations et les consignes sont directement dans le texte du document.







Lexique

suite : Une suite logicielle est ensemble de programmes informatiques regroupés sous une même appellation, offrant des fonctionnalités liées entre elles et possédant souvent une interface graphique plus ou moins similaire ; une suite bureautique servira donc aux opérations basiques du travail de bureau (rédiger des textes, organiser des rendez-vous, comptabiliser). 

formats de document : Lorsqu’un document est sauvegardé il peut être « écrit » par l’ordinateur selon différents codages (qui ne sont pas lisibles par toutes les machines).

extensions (plug-in) : C’est un mini-logiciel qui vient compléter un programme plus gros et lui ajouter des fonctionnalités.

2-clics : comprendre : cliquer 2 fois rapidement sur le bouton gauche de la souris.

Ascenseur : c’est la flèche descendante à droite de cette fenêtre par exemple :
elle permet d’ouvrir un menu déroulant vers d’autres fonctions.

Hyperlien : C’est le plus souvent un renvoi mis en surbrillance dans le texte et activé par « ctrl+clic ». Les hyperliens permettent aux lecteurs d'accéder directement à des informations spécifiques.

URL : «  Une URL, ou « adresse web », est une chaîne de caractères « uniforme » (Uniform Ressource Locator) permettant de « localiser » ou d’identifier une « ressource » du « world wide web » et de préciser le protocole internet pour la récupérer ».


