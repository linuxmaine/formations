# schéma du réseau local 

```mermaid


graph BT;

  classDef classRouteur fill:#f96;   %% Définition du style des routeurs
  classDef classServeur fill:#ff9;   %% Définition du style des Serveurs
  classDef classCPL fill:#f99;   %% Définition du style des CPL
  classDef classSwitch fill:#99F;   %% Définition du style des CPL
  
  cpl0-2 -.- |CPL| cpl1-1
  cpl0-1 -.- |CPL| cpl1-2
  cpl0-3 -.- |CPL| cpl121
  cpl0-3 -.- |CPL| cpl2-2
  

subgraph internet 
    fibre{{bouygues}}
    class fibre classRouteur
end  
  
subgraph Bâtiment
    subgraph "Etage 2"
      subgraph "Asso B01 "
        cpl2-2[\LEA NetPlug85+ /]
        ord210-1((ordi))
        cpl2-2 --- ord210-1
        class cpl2-2 classCPL
      end

      subgraph "Asso B02"
        sw2-1["switch2-1"]
        class sw2-1 classSwitch
        pve2-1("PVE1")
        pve2-2("PVE2")
        class pve2-1,pve2-2 classServeur
        ordi2-1(("ordi"))
        
        sw2-1 --- pve2-1
        sw2-1 --- pve2-2
        sw2-1 --- ordi2-1
      end
    end

    subgraph "Etage 1"
      subgraph "Asso B03"
        cpl1-2[/CPL : DEVICE-FD4A\]
        class cpl1-2 classCPL
        ord1-2((ordi1-2))
        imp1-2((imp1-2))
        swi1-2["swi1-2"]
        class swi1-2 classSwitch
        
        cpl1-2 --- swi1-2
        swi1-2 --- ord1-2
        swi1-2 --- imp1-2
      end

      subgraph "Asso B04"
        cpl1-1[/cpl1-1/]
        class cpl1-1 classCPL
        ord1-1((ordi1-1))
        
        cpl1-1 --- ord1-1
      end
      subgraph "Asso B05"
        cpl121[\LEA cpl121/]
        class cpl121 classCPL
        ord121-1((ordi-1))
        
        cpl121 --- ord121-1
      end
      
    end 
    
    subgraph "Etage 0"
      subgraph "local technique"
        box01{{Bbox}}
        class box01 classRouteur

        sw0-1["swich0-1"]
        class sw0-1 classSwitch
        alcasar("ALCASAR")
        class alcasar classServeur

        cpl0-1[/CPL : DEVICE-574F\]
        cpl0-2[/ CMM /]
        cpl0-3[\ LEA NetPlug 200V+ /]
        class cpl0-1,cpl0-2,cpl0-3 classCPL
        
        fibre === |fibre| box01
        box01 --- |192.168.1.253| alcasar 
        alcasar --- sw0-1
        

        sw0-1 --- cpl0-1
        sw0-1 --- cpl0-2
        sw0-1 --- cpl0-3
        box01 --- sw2-1 

        
      end
      
    end
 end
```
